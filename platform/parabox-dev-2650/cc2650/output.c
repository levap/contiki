/*
 * output.c
 *
 *  Created on: Oct 28, 2016
 *      Author: levap
 */

#include "output.h"
#include "ti-lib.h"

void output_init(void) {
	ti_lib_rom_ioc_pin_type_gpio_output(BOARD_IOID_OUTPUT_1);
	ti_lib_rom_ioc_pin_type_gpio_output(BOARD_IOID_OUTPUT_2);
	ti_lib_rom_ioc_pin_type_gpio_output(BOARD_IOID_OUTPUT_3);
	ti_lib_rom_ioc_pin_type_gpio_output(BOARD_IOID_OUTPUT_4);

	ti_lib_gpio_clear_multi_dio(BOARD_OUTPUTS_ALL);
}
/*---------------------------------------------------------------------------*/
unsigned char outputs_get(void) {
	return 0;
}
/*---------------------------------------------------------------------------*/
void outputs_high(unsigned char output) {
	if(output == BOARD_IOID_OUTPUT_1) {
		ti_lib_gpio_set_dio(BOARD_IOID_OUTPUT_1);
	} else if(output == BOARD_IOID_OUTPUT_2) {
		ti_lib_gpio_set_dio(BOARD_IOID_OUTPUT_2);
	} else if(output == BOARD_IOID_OUTPUT_3) {
		ti_lib_gpio_set_dio(BOARD_IOID_OUTPUT_3);
	} else if(output == BOARD_IOID_OUTPUT_4) {
		ti_lib_gpio_set_dio(BOARD_IOID_OUTPUT_4);
	}
}

void outputs_low(unsigned char output) {
	if(output == BOARD_IOID_OUTPUT_1) {
		ti_lib_gpio_clear_dio(BOARD_IOID_OUTPUT_1);
	} else if(output == BOARD_IOID_OUTPUT_2) {
		ti_lib_gpio_clear_dio(BOARD_IOID_OUTPUT_2);
	} else if(output == BOARD_IOID_OUTPUT_3) {
		ti_lib_gpio_clear_dio(BOARD_IOID_OUTPUT_3);
	} else if(output == BOARD_IOID_OUTPUT_4) {
		ti_lib_gpio_clear_dio(BOARD_IOID_OUTPUT_4);
	}
}
