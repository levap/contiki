/*
 * output.h
 *
 *  Created on: Oct 28, 2016
 *      Author: levap
 */

#ifndef PLATFORM_PARABOX_DEV_DEV_OUTPUT_H_
#define PLATFORM_PARABOX_DEV_DEV_OUTPUT_H_

#include "board.h"

void output_init(void);
unsigned char outputs_get(void);
void outputs_high(unsigned char output);
void outputs_low(unsigned char output);

#endif /* PLATFORM_PARABOX_DEV_DEV_OUTPUT_H_ */
