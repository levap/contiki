/*
 * input.c
 *
 *  Created on: Oct 29, 2016
 *      Author: levap
 */

//INPUT_SK1_PORT
//INPUT_SK1_PIN
//
//INPUT_SK2_PORT
//INPUT_SK2_PIN
//
//INPUT_220_PORT
//INPUT_220_PIN

#include "input.h"
#include <uip-debug.h>

uint16_t input_sk1_accum = 0;
uint32_t input_sk1_laststate = 0;

uint16_t input_sk2_accum = 0;
uint32_t input_sk2_laststate = 0;

uint16_t input_220_accum = 0;
uint32_t input_220_laststate = 0;

input_call_back call_back = 0;

void config_pin(uint32_t port_base, uint32_t pin_mask) {
	  GPIO_SOFTWARE_CONTROL(port_base, pin_mask);
	  GPIO_SET_INPUT(port_base, pin_mask);
}

void input_init() {
	config_pin(GPIO_PORT_TO_BASE(INPUT_SK1_PORT), GPIO_PIN_MASK(INPUT_SK1_PIN));
	config_pin(GPIO_PORT_TO_BASE(INPUT_SK2_PORT), GPIO_PIN_MASK(INPUT_SK2_PIN));
	config_pin(GPIO_PORT_TO_BASE(INPUT_220_PORT), GPIO_PIN_MASK(INPUT_220_PIN));
}

void input_set_callback(input_call_back cb) {
	call_back = cb;
}

void input_read(void) {
	uint32_t currentStateSK1mask = GPIO_READ_PIN(GPIO_PORT_TO_BASE(INPUT_SK1_PORT), GPIO_PIN_MASK(INPUT_SK1_PIN));
	uint32_t currentStateSK2mask = GPIO_READ_PIN(GPIO_PORT_TO_BASE(INPUT_SK2_PORT), GPIO_PIN_MASK(INPUT_SK2_PIN));
	uint32_t currentState220mask = GPIO_READ_PIN(GPIO_PORT_TO_BASE(INPUT_220_PORT), GPIO_PIN_MASK(INPUT_220_PIN));

	uint32_t currentStateSK1 = (currentStateSK1mask > 0) ? 1 : 0;
	uint32_t currentStateSK2 = (currentStateSK2mask > 0) ? 1 : 0;
	uint32_t currentState220 = (currentState220mask > 0) ? 1 : 0;

	input_sk1_accum <<= 1;
	input_sk2_accum <<= 1;
	input_220_accum <<= 1;

	input_sk1_accum |= currentStateSK1;
	input_sk2_accum |= currentStateSK2;
	input_220_accum |= currentState220;

	if((input_sk1_accum == 0xFFFF) || (input_sk1_accum == 0)) {
		if(currentStateSK1 != input_sk1_laststate) {
			PRINTF("Input sk1\n");
			input_sk1_laststate = currentStateSK1;
			if(call_back != 0) {
				call_back(0, currentStateSK1);
			}
		}
	}
	if((input_sk2_accum == 0xFFFF) || (input_sk2_accum == 0)) {
		if(currentStateSK2 != input_sk2_laststate) {
			PRINTF("Input sk2\n");
			input_sk2_laststate = currentStateSK2;
			if(call_back != 0) {
				call_back(1, currentStateSK2);
			}
		}
	}
	if((input_220_accum == 0xFFFF) || (input_220_accum == 0)) {
		if(currentState220 != input_220_laststate) {
			PRINTF("Input 220\n");
			input_220_laststate = currentState220;
			if(call_back != 0) {
				call_back(2, currentState220);
			}
		}
	}
}

uint8_t input_get(uint8_t index) {
	if(index == 0) {
		return (uint8_t) input_sk1_laststate;
	} else if(index == 1) {
		return (uint8_t) input_sk2_laststate;
	} else if(index == 2) {
		return (uint8_t) input_220_laststate;
	} else {
		return 0;
	}
}
