/*
 * output.c
 *
 *  Created on: Oct 28, 2016
 *      Author: levap
 */

#include "output.h"

#define OUTPUTS_PORT_BASE    GPIO_PORT_TO_BASE(OUTPUTS_PORT)

void output_init(void) {
	GPIO_SET_OUTPUT(OUTPUTS_PORT_BASE, OUTPUTS_ALL);
	GPIO_SOFTWARE_CONTROL(OUTPUTS_PORT_BASE, OUTPUTS_ALL);
}
/*---------------------------------------------------------------------------*/
unsigned char outputs_get(void) {
	return GPIO_READ_PIN(OUTPUTS_PORT_BASE, OUTPUTS_ALL);
}
/*---------------------------------------------------------------------------*/
void outputs_high(unsigned char output) {
	GPIO_SET_PIN(OUTPUTS_PORT_BASE, GPIO_PIN_MASK(output)); //HIGH
}

void outputs_low(unsigned char output) {
	GPIO_CLR_PIN(OUTPUTS_PORT_BASE, GPIO_PIN_MASK(output)); //LOW
}
