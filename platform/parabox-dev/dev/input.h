/*
 * input.h
 *
 *  Created on: Oct 29, 2016
 *      Author: levap
 */

#ifndef PLATFORM_PARABOX_DEV_DEV_INPUT_H_
#define PLATFORM_PARABOX_DEV_DEV_INPUT_H_

#include <string.h>
#include "board.h"

typedef void (*input_call_back) (uint32_t, uint32_t);

void input_init();
void input_set_callback(input_call_back call_back);
void input_read(void);
uint8_t input_get(uint8_t index);

#endif /* PLATFORM_PARABOX_DEV_DEV_INPUT_H_ */
