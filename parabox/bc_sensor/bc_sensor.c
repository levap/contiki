
#include "contiki.h"
#include "contiki-lib.h"

#include "sys/etimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
//#include "batmon-sensor.h"
#include "dev/leds.h"

#include "simple-udp.h"

#include "ti-lib.h"


#include <stdio.h>
#include <string.h>

/*---------------------------------------------------------------------------*/
#define UDP_PORT 12345

#define SEND_INTERVAL		(30 * CLOCK_SECOND)

static struct simple_udp_connection broadcast_connection;

/*---------------------------------------------------------------------------*/
static void
receiver(struct simple_udp_connection *c,
         const uip_ipaddr_t *sender_addr,
         uint16_t sender_port,
         const uip_ipaddr_t *receiver_addr,
         uint16_t receiver_port,
         const uint8_t *data,
         uint16_t datalen)
{
  printf("Data received on port: %d from port: %d with length: %d\n", receiver_port, sender_port, datalen);
}

/*---------------------------------------------------------------------------*/
PROCESS(broadcast_example_process, "UDP broadcast process");
AUTOSTART_PROCESSES(&broadcast_example_process);

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(broadcast_example_process, ev, data) {
	static struct etimer periodic_timer;
	uip_ipaddr_t addr;

	PROCESS_BEGIN();

//	SENSORS_ACTIVATE(batmon_sensor);

	simple_udp_register(&broadcast_connection, UDP_PORT, NULL, UDP_PORT, receiver);

	etimer_set(&periodic_timer, SEND_INTERVAL);
	while(1) {
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&periodic_timer));
		etimer_reset(&periodic_timer);

		printf("Sending broadcast\n");

		leds_on(BOARD_LED_1);
		NETSTACK_MAC.on();

		uip_create_linklocal_allnodes_mcast(&addr);
		simple_udp_sendto(&broadcast_connection, "TEST", 4, &addr);

		leds_off(BOARD_LED_1);
		NETSTACK_MAC.off(0);
	}

	PROCESS_END();
}
/*---------------------------------------------------------------------------*/
