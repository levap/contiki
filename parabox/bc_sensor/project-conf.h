/*
 * Copyright (c) 2015, Texas Instruments Incorporated - http://www.ti.com/
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*---------------------------------------------------------------------------*/
#ifndef PROJECT_CONF_H_
#define PROJECT_CONF_H_
/*---------------------------------------------------------------------------*/
//-------------------------------------------------------------------------//
// MY CONFIG START
#undef NETSTACK_CONF_RDC
//#define NETSTACK_CONF_RDC     contikimac_driver
#define NETSTACK_CONF_RDC     nullrdc_driver

#undef NETSTACK_CONF_MAC
#define NETSTACK_CONF_MAC     csma_driver

//#undef LLSEC802154_CONF_ENABLED
//#define LLSEC802154_CONF_ENABLED          1
//
//#undef NETSTACK_CONF_FRAMER
//#define NETSTACK_CONF_FRAMER              noncoresec_framer
//
//#undef NETSTACK_CONF_LLSEC
//#define NETSTACK_CONF_LLSEC               noncoresec_driver
//
//#undef NONCORESEC_CONF_SEC_LVL
//#define NONCORESEC_CONF_SEC_LVL           0x02
//
//#define NONCORESEC_CONF_KEY { 0x67 , 0x53 , 0x11 , 0x9A , 0x6B , 0x05 , 0xEF , 0xC3 , 0x08 , 0x9A , 0x4C , 0xAA , 0x2C , 0x7D , 0x2E , 0x1F }

//#define LPM_CONF_ENABLE       0
//#define LPM_CONF_MAX_PM       1

#define IEEE802154_CONF_PANID           0x2222
#define RF_CORE_CONF_CHANNEL              	18
//-------------------------------------------------------------------------//
// MY CONFIG END
/*---------------------------------------------------------------------------*/
/* Enable the ROM bootloader */
#define ROM_BOOTLOADER_ENABLE                 1
/*---------------------------------------------------------------------------*/
/* For very sleepy operation */
#define RF_BLE_CONF_ENABLED                   0
#define UIP_DS6_CONF_PERIOD        CLOCK_SECOND
#define UIP_CONF_TCP                          0
#define RPL_CONF_LEAF_ONLY                    1

/*
 * We'll fail without RPL probing, so turn it on explicitly even though it's
 * on by default
 */
#define RPL_CONF_WITH_PROBING                 0
/*---------------------------------------------------------------------------*/
#endif /* PROJECT_CONF_H_ */
/*---------------------------------------------------------------------------*/
