/*
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"

#include "sys/etimer.h"
#include "sys/stimer.h"
#include "sys/process.h"
#include "dev/leds.h"
#include "dev/watchdog.h"
#include "button-sensor.h"
#include "batmon-sensor.h"
#include "net/netstack.h"
#include "net/ipv6/uip-ds6-nbr.h"
#include "net/ipv6/uip-ds6-route.h"
#include "net/rpl/rpl.h"
#include "net/rpl/rpl-private.h"

#include "ti-lib.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "../protocol/protocol.h"

/*---------------------------------------------------------------------------*/
/* uIP */
#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
#define UIP_UDP_BUF  ((struct uip_udp_hdr *)&uip_buf[uip_l2_l3_hdr_len])
static struct uip_udp_conn *server_conn;

static const char *server_ip_string = "fd00::1";
static uip_ip6addr_t server_ip6addr;
#define SERVER_PORT 	3001
#define LOCAL_PORT		3000

#define MAX_PAYLOAD_LEN 256
static unsigned char outputBuffer[MAX_PAYLOAD_LEN];

/*---------------------------------------------------------------------------*/
/* Normal mode duration params in seconds */
#define NORMAL_OP_DURATION_DEFAULT 3
/*---------------------------------------------------------------------------*/
/* Observer notification period params in seconds */
#define PERIODIC_INTERVAL_DEFAULT  60
/*---------------------------------------------------------------------------*/
#define VERY_SLEEPY_MODE_OFF 0
#define VERY_SLEEPY_MODE_ON  1
/*---------------------------------------------------------------------------*/
#define MAC_CAN_BE_TURNED_OFF  0
#define MAC_MUST_STAY_ON       1

#define KEEP_MAC_ON_MIN_PERIOD 3 /* secs */
/*---------------------------------------------------------------------------*/
#define PERIODIC_INTERVAL         CLOCK_SECOND
/*---------------------------------------------------------------------------*/
typedef struct sleepy_config_s {
	unsigned long interval;
	unsigned long duration;
	uint8_t mode;
} sleepy_config_t;

sleepy_config_t config;
/*---------------------------------------------------------------------------*/
#define STATE_NORMAL           0
#define STATE_NOTIFY_OBSERVERS 1
#define STATE_VERY_SLEEPY      2
/*---------------------------------------------------------------------------*/
static struct stimer st_duration;
static struct stimer st_interval;
static struct stimer st_min_mac_on_duration;
static struct etimer et_periodic;
static process_event_t event_new_config;
static uint8_t state;

/*---------------------------------------------------------------------------*/
PROCESS(very_sleepy_demo_process, "CC13xx/CC26xx very sleepy process");
AUTOSTART_PROCESSES(&very_sleepy_demo_process);

/*---------------------------------------------------------------------------*/
static void tcpip_write(const void *data, int len, int sporadic) {
	if (sporadic > 0) {
		uip_ipaddr_copy(&server_conn->ripaddr, &server_ip6addr);
		server_conn->rport = UIP_HTONS(SERVER_PORT);
	} else {
		uip_ipaddr_copy(&server_conn->ripaddr, &UIP_IP_BUF->srcipaddr);
		server_conn->rport = UIP_UDP_BUF->srcport;
	}
	uip_udp_packet_send(server_conn, data, len);
	/* Restore server connection to allow data from any node */
	uip_create_unspecified(&server_conn->ripaddr);
	server_conn->rport = 0;
}
/*---------------------------------------------------------------------------*/
static void tcpip_write_sporadic(uint16_t param, uint8_t index,
		uint8_t dataType, uint8_t dataLen, uint32_t data) {
	struct ParaboxHeader messageHeader;
	messageHeader.version = 1;
	messageHeader.action = ACTION_SPORADIC;
	messageHeader.parameter = param;
	messageHeader.index = index;
	messageHeader.dataType = dataType;
	messageHeader.dataLen = dataLen;
	memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));
	int i = 0;
	for (i = 0; i < dataLen; ++i) {
		outputBuffer[sizeof(messageHeader) + i] = (data >> i * 8) & 0xFF;
	}
	tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 1);
}

/*---------------------------------------------------------------------------*/
/*
 * If our preferred parent is not NBR_REACHABLE in the ND cache, NUD will send
 * a unicast NS and wait for NA. If NA fails then the neighbour will be removed
 * from the ND cache and the default route will be deleted. To prevent this,
 * keep the MAC on until the parent becomes NBR_REACHABLE. We also keep the MAC
 * on if we are about to do RPL probing.
 *
 * In all cases, the radio will be locked on for KEEP_MAC_ON_MIN_PERIOD secs
 */
static uint8_t keep_mac_on(void) {
	uip_ds6_nbr_t *nbr;
	uint8_t rv = MAC_CAN_BE_TURNED_OFF;

	if (!stimer_expired(&st_min_mac_on_duration)) {
		return MAC_MUST_STAY_ON;
	}

#if RPL_WITH_PROBING
	/* Determine if we are about to send a RPL probe */
	if (CLOCK_LT(
			etimer_expiration_time(
					&rpl_get_default_instance()->probing_timer.etimer),
			(clock_time() + PERIODIC_INTERVAL))) {
		rv = MAC_MUST_STAY_ON;
	}
#endif

	/* It's OK to pass a NULL pointer, the callee checks and returns NULL */
	nbr = uip_ds6_nbr_lookup(uip_ds6_defrt_choose());

	if (nbr == NULL) {
		/* We don't have a default route, or it's not reachable (NUD likely). */
		rv = MAC_MUST_STAY_ON;
	} else {
		if (nbr->state != NBR_REACHABLE) {
			rv = MAC_MUST_STAY_ON;
		}
	}

	if (rv == MAC_MUST_STAY_ON && stimer_expired(&st_min_mac_on_duration)) {
		stimer_set(&st_min_mac_on_duration, KEEP_MAC_ON_MIN_PERIOD);
	}

	return rv;
}
/*---------------------------------------------------------------------------*/
static void switch_to_normal(void) {
	state = STATE_NOTIFY_OBSERVERS;

	/*
	 * Stay in normal mode for 'duration' secs.
	 * Transition back to normal in 'interval' secs, _including_ 'duration'
	 */
	stimer_set(&st_duration, config.duration);
	stimer_set(&st_interval, config.interval);
}
/*---------------------------------------------------------------------------*/
static void switch_to_very_sleepy(void) {
	state = STATE_VERY_SLEEPY;
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(very_sleepy_demo_process, ev, data) {
	uint8_t mac_keep_on;

	PROCESS_BEGIN();

	SENSORS_ACTIVATE(batmon_sensor);

	config.mode = VERY_SLEEPY_MODE_ON;
	config.interval = PERIODIC_INTERVAL_DEFAULT;
	config.duration = NORMAL_OP_DURATION_DEFAULT;

	state = STATE_NORMAL;

	event_new_config = process_alloc_event();

	printf("Very Sleepy Demo Process\n");

	uiplib_ip6addrconv(server_ip_string, &server_ip6addr);
	server_conn = udp_new(NULL, UIP_HTONS(0), NULL);

	switch_to_normal();

	etimer_set(&et_periodic, PERIODIC_INTERVAL);

	while (1) {

		PROCESS_YIELD();

		if (ev == sensors_event && data == &button_start_sensor) {
			switch_to_normal();
		}

		if (ev == event_new_config) {
			stimer_set(&st_interval, config.interval);
			stimer_set(&st_duration, config.duration);
		}

		if ((ev == PROCESS_EVENT_TIMER && data == &et_periodic)
				|| (ev == sensors_event && data == &button_start_sensor)
				|| (ev == event_new_config)) {

			/*
			 * Determine if the stack is about to do essential network maintenance
			 * and, if so, keep the MAC layer on
			 */
			mac_keep_on = keep_mac_on();

			if (mac_keep_on == MAC_MUST_STAY_ON || state != STATE_VERY_SLEEPY) {
				leds_on(BOARD_LED_1);
				NETSTACK_MAC.on();
			}

			/*
			 * Next, switch between normal and very sleepy mode depending on config,
			 * send notifications to observers as required.
			 */
			if (state == STATE_NOTIFY_OBSERVERS) {
				int32_t temp = batmon_sensor.value(BATMON_SENSOR_TYPE_TEMP);
				int32_t voltage = (batmon_sensor.value(BATMON_SENSOR_TYPE_VOLT) * 125) >> 5;

				// WRITE TO SERVER
				tcpip_write_sporadic(PARAM_TEMP, 0, DATATYPE_NUMBER, 4, temp);
				tcpip_write_sporadic(PARAM_VOLTAGE, 0, DATATYPE_NUMBER, 4, voltage);
				tcpip_write_sporadic(PARAM_UPTIME, 0, DATATYPE_NUMBER, 4, clock_seconds());

				state = STATE_NORMAL;
			}

			if (state == STATE_NORMAL) {
				if (stimer_expired(&st_duration)) {
					stimer_set(&st_duration, config.duration);
					if (config.mode == VERY_SLEEPY_MODE_ON) {
						switch_to_very_sleepy();
					}
				}
			} else if (state == STATE_VERY_SLEEPY) {
				if (stimer_expired(&st_interval)) {
					switch_to_normal();
				}
			}

			if (mac_keep_on == MAC_CAN_BE_TURNED_OFF && state == STATE_VERY_SLEEPY) {
				leds_off(BOARD_LED_1);
				NETSTACK_MAC.off(0);
			} else {
				leds_on(BOARD_LED_1);
				NETSTACK_MAC.on();
			}

			/* Schedule next pass */
			etimer_set(&et_periodic, PERIODIC_INTERVAL);
		}
	}

PROCESS_END();
}
/*---------------------------------------------------------------------------*/
