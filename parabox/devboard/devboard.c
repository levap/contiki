#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"

#include <string.h>

#define DEBUG DEBUG_PRINT

//#define PLATFORM_PARABOX_DEV_HAS_CO2	1
//#define PLATFORM_PARABOX_DEV_HAS_DHT22	1

#define RECEIVE_BROADCAST				1

#include "net/ip/uip-debug.h"
#include "dev/flash.h"
#include "dev/rom-util.h"
#include "dev/watchdog.h"

#ifdef PLATFORM_PARABOX_DEV_HAS_DHT22
#include "dev/dht22.h"
#endif

#include "cpu.h"
#include "cfs-coffee-arch.h"
#include "dev/leds.h"
#include "dev/uart.h"
#include "button-sensor.h"
#include "dev/cc2538-sensors.h"
#include "input.h"
#include "output.h"
#include "net/rpl/rpl.h"
#include "net/ipv6/uip-icmp6.h"
#include "net/ipv6/sicslowpan.h"

#ifdef RECEIVE_BROADCAST
#include "simple-udp.h"
#endif

#include "../protocol/protocol.h"

/*---------------------------------------------------------------------------*/
#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
#define UIP_UDP_BUF  ((struct uip_udp_hdr *)&uip_buf[uip_l2_l3_hdr_len])

/* Payload length of ICMPv6 echo requests used to measure RSSI with def rt */
#define ECHO_REQ_PAYLOAD_LEN   20

#define MAX_PAYLOAD_LEN 256
/*---------------------------------------------------------------------------*/
static struct uip_udp_conn *server_conn;
static unsigned char inputBuffer[MAX_PAYLOAD_LEN];
static unsigned char outputBuffer[MAX_PAYLOAD_LEN];
static uint16_t len;
/*---------------------------------------------------------------------------*/
/* Parent RSSI functionality */
static struct uip_icmp6_echo_reply_notification echo_reply_notification;
static struct etimer echo_request_timer;
static int def_rt_rssi = 0;
#define DEFAULT_RSSI_MEAS_INTERVAL		(CLOCK_SECOND * 60)
/*---------------------------------------------------------------------------*/
PROCESS(udp_echo_server_process, "UDP Server Process");
AUTOSTART_PROCESSES(&udp_echo_server_process);
/*---------------------------------------------------------------------------*/
static const char *server_ip_string = "fd00::1";
static uip_ip6addr_t server_ip6addr;
#define SERVER_PORT 	3001
#define LOCAL_PORT		3000

#define THING_VERSION	"01.01:02:00"

/*---------------------------------------------------------------------------*/
// TELEMETRY
/*---------------------------------------------------------------------------*/
static struct etimer telemetry_send_timer;
#define DEFAULT_TELEMETRY_SEND_INTERVAL		(CLOCK_SECOND * 600)

/*---------------------------------------------------------------------------*/
// INPUTS
/*---------------------------------------------------------------------------*/
#define INPUT_BOOT &button_boot_sensor

static struct etimer input_read_timer;
#define DEFAULT_INPUT_READ_INTERVAL		(CLOCK_SECOND / 8)

/*---------------------------------------------------------------------------*/
// SENSORS
/*---------------------------------------------------------------------------*/
static struct etimer sensor_read_timer;
#define DEFAULT_SENSOR_READ_INTERVAL		(CLOCK_SECOND * 60)

#ifdef PLATFORM_PARABOX_DEV_HAS_DHT22
	int32_t dht22_temperature = 0;
	int32_t dht22_humidity = 0;
#endif

#ifdef PLATFORM_PARABOX_DEV_HAS_CO2
	uint32_t co2_concentration = 0;
#endif

#ifdef RECEIVE_BROADCAST
/*---------------------------------------------------------------------------*/
// Broadcast
/*---------------------------------------------------------------------------*/
#define UDP_BROADCAST_PORT 12345
static struct simple_udp_connection broadcast_connection;

/*---------------------------------------------------------------------------*/
static void broadcast_receiver(struct simple_udp_connection *c, //
         const uip_ipaddr_t *sender_addr, //
         uint16_t sender_port, //
         const uip_ipaddr_t *receiver_addr, //
         uint16_t receiver_port, //
         const uint8_t *data, //
         uint16_t datalen) {

	PRINTF("Broadcast received on port: %d from port: %d with length: %d\n", receiver_port, sender_port, datalen);
}
#endif

/*---------------------------------------------------------------------------*/
// BOOTLOADER
/*---------------------------------------------------------------------------*/
extern const flash_cca_lock_page_t flash_cca_lock_page;

static void update_cca_startup_address(uint32_t addr) {
	flash_cca_lock_page_t flash_cca_saved = flash_cca_lock_page;
	uint32_t sector_start = ((uint32_t) &flash_cca_lock_page) & ~(FLASH_PAGE_SIZE - 1);
	flash_cca_saved.app_entry_point = (const void *) addr;

	INTERRUPTS_DISABLE();
	rom_util_page_erase(sector_start, FLASH_PAGE_SIZE);
	rom_util_program_flash((uint32_t *) &flash_cca_saved, (uint32_t) &flash_cca_lock_page, sizeof(flash_cca_lock_page_t));
	INTERRUPTS_ENABLE();
}
/*---------------------------------------------------------------------------*/
static struct ctimer reboot_timer;

static void do_the_reboot(void *ptr) {
	watchdog_reboot();
}

static void start_bootloader() {
	uint32_t bootloader_address = COFFEE_START + COFFEE_SIZE;

	/* Note: if there is no bootloader at this address the device will be bricked */
	update_cca_startup_address(bootloader_address);

	ctimer_set(&reboot_timer, CLOCK_SECOND, do_the_reboot, NULL);
	PRINTF("Device will reboot to bootloader starting at: 0x%lx!", bootloader_address);
}

/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
static void echo_reply_handler(uip_ipaddr_t *source, uint8_t ttl, uint8_t *data, uint16_t datalen) {
	if(uip_ip6addr_cmp(source, uip_ds6_defrt_choose())) {
		def_rt_rssi = sicslowpan_get_last_rssi();
		PRINTF("Ping RSSI: %d TTL: %d\n", def_rt_rssi, ttl);
	}
}

/*---------------------------------------------------------------------------*/
static void ping_parent(void) {
	if(uip_ds6_get_global(ADDR_PREFERRED) == NULL) {
		return;
	}
	uip_icmp6_send(uip_ds6_defrt_choose(), ICMP6_ECHO_REQUEST, 0, ECHO_REQ_PAYLOAD_LEN);
}
/*---------------------------------------------------------------------------*/
static void tcpip_write(const void *data, int len, int sporadic) {
	if(sporadic > 0) {
		uip_ipaddr_copy(&server_conn->ripaddr, &server_ip6addr);
		server_conn->rport = UIP_HTONS(SERVER_PORT);
	} else {
		uip_ipaddr_copy(&server_conn->ripaddr, &UIP_IP_BUF->srcipaddr);
		server_conn->rport = UIP_UDP_BUF->srcport;
	}
	uip_udp_packet_send(server_conn, data, len);
	/* Restore server connection to allow data from any node */
	uip_create_unspecified(&server_conn->ripaddr);
	server_conn->rport = 0;
}

/*---------------------------------------------------------------------------*/
static void tcpip_write_error(struct ParaboxHeader *messageHeader, uint8_t errorCode) {
	if(messageHeader->action == ACTION_WRITE) {
		messageHeader->action = ACTION_WRITE_ACK;
	} else if(messageHeader->action == ACTION_READ) {
		messageHeader->action = ACTION_READ_ACK;
	}
	messageHeader->dataType = DATATYPE_ERROR;
	messageHeader->dataLen = 1;
	memcpy(outputBuffer, messageHeader, sizeof(*messageHeader));
	outputBuffer[sizeof(*messageHeader)] = errorCode;

	tcpip_write(outputBuffer, sizeof(*messageHeader) + messageHeader->dataLen, 0);
}

/*---------------------------------------------------------------------------*/
static void tcpip_write_sporadic(uint16_t param, uint8_t index, uint8_t dataType, uint8_t dataLen, uint32_t data) {
	struct ParaboxHeader messageHeader;
	messageHeader.version = 1;
	messageHeader.action = ACTION_SPORADIC;
	messageHeader.parameter = param;
	messageHeader.index = index;
	messageHeader.dataType = dataType;
	messageHeader.dataLen = dataLen;
	memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));
	int i = 0;
	for(i = 0; i < dataLen; ++i) {
		outputBuffer[sizeof(messageHeader) + i] = (data >> i * 8) & 0xFF;
	}
	tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 1);
}

/*---------------------------------------------------------------------------*/
static void tcpip_handler(void) {
	struct ParaboxHeader messageHeader;
	memset(inputBuffer, 0, MAX_PAYLOAD_LEN);
	if (uip_newdata()) {
		len = uip_datalen();
		memcpy(inputBuffer, uip_appdata, len);
		PRINTF("%u bytes from [", len);
		PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
		PRINTF("]:%u\n", UIP_HTONS(UIP_UDP_BUF->srcport));
		if (len >= sizeof(messageHeader)) {
			memcpy(&messageHeader, inputBuffer, sizeof(messageHeader));
			switch(messageHeader.parameter) {
			case PARAM_INPUT:
				if (messageHeader.action == ACTION_READ) {
					if(messageHeader.index > 2) {
						tcpip_write_error(&messageHeader, ERROR_WRONG_INDEX);
						return;
					}
					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_NUMBER;
					messageHeader.dataLen = 1;

					uint8_t value = input_get(messageHeader.index);

					memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));
					outputBuffer[sizeof(messageHeader)] = value;

					tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 0);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_OUTPUT:
				if (messageHeader.action == ACTION_WRITE) {
					uint8_t output = 0;
					if (messageHeader.index == 0) {
						output = OUTPUT1;
					} else if (messageHeader.index == 1) {
						output = OUTPUT2;
					} else if (messageHeader.index == 2) {
						output = OUTPUT3;
					} else if (messageHeader.index == 3) {
						output = OUTPUT4;
					} else {
						tcpip_write_error(&messageHeader, ERROR_WRONG_INDEX);
						return;
					}
					uint8_t value = inputBuffer[sizeof(messageHeader)];
					if (value == 0) {
						PRINTF("Turn off output %u\n", output);
						outputs_low(output);
					} else {
						PRINTF("Turn on output %u\n", output);
						outputs_high(output);
					}
					messageHeader.action = ACTION_WRITE_ACK;
					messageHeader.dataType = DATATYPE_VOID;
					messageHeader.dataLen = 0;

					tcpip_write(&messageHeader, sizeof(messageHeader), 0);
				}
				if (messageHeader.action == ACTION_READ) {
					uint8_t status = 0;
					uint8_t output = 0;
					if (messageHeader.index == 0) {
						output = OUTPUT1;
					} else if (messageHeader.index == 1) {
						output = OUTPUT2;
					} else if (messageHeader.index == 2) {
						output = OUTPUT3;
					} else if (messageHeader.index == 3) {
						output = OUTPUT4;
					} else {
						tcpip_write_error(&messageHeader, ERROR_WRONG_INDEX);
						return;
					}

					uint8_t currentOutputs = outputs_get();
					if((currentOutputs & output) > 0) {
						status = 1;
					} else {
						status = 0;
					}

					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_NUMBER;
					messageHeader.dataLen = 1;

					memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));
					outputBuffer[sizeof(messageHeader)] = status;

					tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 0);
				}
				break;
			case PARAM_VERSION:
				if (messageHeader.action == ACTION_READ) {
					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_STRING;
					messageHeader.dataLen = sizeof(THING_VERSION);

					memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));
					int i = 0;
					for(i = 0; i < sizeof(THING_VERSION); ++i) {
						outputBuffer[sizeof(messageHeader) + i] = THING_VERSION[i];
					}

					tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 0);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_RSSI:
				if (messageHeader.action == ACTION_READ) {
					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_NUMBER;
					messageHeader.dataLen = 4;

					memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));
					outputBuffer[sizeof(messageHeader)] = def_rt_rssi & 0xFF;
					outputBuffer[sizeof(messageHeader) + 1] = (def_rt_rssi >> 8) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 2] = (def_rt_rssi >> 16) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 3] = (def_rt_rssi >> 24) & 0xFF;

					tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 0);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_UPTIME:
				if (messageHeader.action == ACTION_READ) {
					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_NUMBER;
					messageHeader.dataLen = 4;

					memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));
					unsigned long uptime = clock_seconds();
					outputBuffer[sizeof(messageHeader)] = uptime & 0xFF;
					outputBuffer[sizeof(messageHeader) + 1] = (uptime >> 8) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 2] = (uptime >> 16) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 3] = (uptime >> 24) & 0xFF;

					tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 0);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_ADC:
				tcpip_write_error(&messageHeader, ERROR_NOT_IMPLEMENTED);
				break;
			case PARAM_TEMP:
				if (messageHeader.action == ACTION_READ) {
					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_NUMBER;
					messageHeader.dataLen = 4;

					memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));

					int chip_temp = cc2538_temp_sensor.value(CC2538_SENSORS_VALUE_TYPE_CONVERTED);
					PRINTF("On-Chip Temp (C): %d", chip_temp);

					outputBuffer[sizeof(messageHeader)] = chip_temp & 0xFF;
					outputBuffer[sizeof(messageHeader) + 1] = (chip_temp >> 8) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 2] = (chip_temp >> 16) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 3] = (chip_temp >> 24) & 0xFF;

					tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 0);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_RESTART:
				if (messageHeader.action == ACTION_WRITE) {
					messageHeader.action = ACTION_WRITE_ACK;
					messageHeader.dataType = DATATYPE_VOID;
					messageHeader.dataLen = 0;
					tcpip_write(outputBuffer, sizeof(messageHeader), 0);

					ctimer_set(&reboot_timer, CLOCK_SECOND * 2, do_the_reboot, NULL);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_START_BOOTLOADER:
				if (messageHeader.action == ACTION_WRITE) {
					messageHeader.action = ACTION_WRITE_ACK;
					messageHeader.dataType = DATATYPE_VOID;
					messageHeader.dataLen = 0;
					tcpip_write(outputBuffer, sizeof(messageHeader), 0);

					start_bootloader();
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_SERIAL1_WRITE:
				tcpip_write_error(&messageHeader, ERROR_NOT_IMPLEMENTED);
				break;
			case PARAM_SERIAL2_WRITE:
				tcpip_write_error(&messageHeader, ERROR_NOT_IMPLEMENTED);
				break;
			case PARAM_SERIAL1_READ:
				tcpip_write_error(&messageHeader, ERROR_NOT_IMPLEMENTED);
				break;
			case PARAM_SERIAL2_READ:
				tcpip_write_error(&messageHeader, ERROR_NOT_IMPLEMENTED);
				break;
#ifdef PLATFORM_PARABOX_DEV_HAS_CO2
			case PARAM_CO2_CONCENTRATION:
				if (messageHeader.action == ACTION_READ) {
					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_NUMBER;
					messageHeader.dataLen = 4;

					memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));

					outputBuffer[sizeof(messageHeader)] = co2_concentration & 0xFF;
					outputBuffer[sizeof(messageHeader) + 1] = (co2_concentration >> 8) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 2] = (co2_concentration >> 16) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 3] = (co2_concentration >> 24) & 0xFF;

					tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 0);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
#endif
#ifdef PLATFORM_PARABOX_DEV_HAS_DHT22
			case PARAM_TEMPERATURE:
				if (messageHeader.action == ACTION_READ) {
					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_NUMBER;
					messageHeader.dataLen = 4;

					memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));

					outputBuffer[sizeof(messageHeader)] = dht22_temperature & 0xFF;
					outputBuffer[sizeof(messageHeader) + 1] = (dht22_temperature >> 8) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 2] = (dht22_temperature >> 16) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 3] = (dht22_temperature >> 24) & 0xFF;

					tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 0);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_HUMIDITY:
				if (messageHeader.action == ACTION_READ) {
					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_NUMBER;
					messageHeader.dataLen = 4;

					memcpy(outputBuffer, &messageHeader, sizeof(messageHeader));

					outputBuffer[sizeof(messageHeader)] = dht22_humidity & 0xFF;
					outputBuffer[sizeof(messageHeader) + 1] = (dht22_humidity >> 8) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 2] = (dht22_humidity >> 16) & 0xFF;
					outputBuffer[sizeof(messageHeader) + 3] = (dht22_humidity >> 24) & 0xFF;

					tcpip_write(outputBuffer, sizeof(messageHeader) + messageHeader.dataLen, 0);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
#endif
			}
		}
	}
	return;
}

/*---------------------------------------------------------------------------*/
static void input_fire_handler(uint32_t index, uint32_t value) {
	tcpip_write_sporadic(PARAM_INPUT, (uint8_t) index, DATATYPE_NUMBER, 1, value);
}

/*---------------------------------------------------------------------------*/
static void print_local_addresses(void) {
	int i;
	uint8_t state;
	PRINTF("Local IPv6 addresses:\n");
	for (i = 0; i < UIP_DS6_ADDR_NB; i++) {
		state = uip_ds6_if.addr_list[i].state;
		if (uip_ds6_if.addr_list[i].isused
				&& (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
			PRINTF("  ");PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);PRINTF("\n");
			if (state == ADDR_TENTATIVE) {
				uip_ds6_if.addr_list[i].state = ADDR_PREFERRED;
			}
		}
	}
}

/*---------------------------------------------------------------------------*/
// DHT22 Sensor
/*---------------------------------------------------------------------------*/
#ifdef PLATFORM_PARABOX_DEV_HAS_DHT22

#define SENSOR_AVR_LEN	4

int32_t dht22_temperature_series[SENSOR_AVR_LEN];
int32_t dht22_humidity_series[SENSOR_AVR_LEN];
uint8_t dht22_data_index = 0;

static void processDHT22Sensor() {
	int temperature, humidity;
	if(dht22_read_all(&temperature, &humidity) != DHT22_ERROR) {
		PRINTF("Temperature series [%u] %02d.%02d C, ", dht22_data_index, temperature / 10, temperature % 10);
		PRINTF("Humidity series [%u] %02d.%02d RH\n", dht22_data_index, humidity / 10, humidity % 10);

		dht22_temperature_series[dht22_data_index] = temperature;
		dht22_humidity_series[dht22_data_index] = humidity;
		dht22_data_index++;

		if(dht22_data_index == SENSOR_AVR_LEN) {
			int32_t avr_temp = 0;
			uint8_t i = 0;
			for(i = 0; i < SENSOR_AVR_LEN; i++) {
				avr_temp += dht22_temperature_series[i];
			}
			dht22_temperature = avr_temp / SENSOR_AVR_LEN;
			PRINTF("AVR Temperature %02ld.%02ld C, ", dht22_temperature / 10, dht22_temperature % 10);

			int32_t avr_humidity = 0;
			for(i = 0; i < SENSOR_AVR_LEN; i++) {
				avr_humidity += dht22_humidity_series[i];
			}
			dht22_humidity = avr_humidity / SENSOR_AVR_LEN;
			PRINTF("AVR Humidity %02ld.%02ld RH\n", dht22_humidity / 10, dht22_humidity % 10);

			tcpip_write_sporadic(PARAM_TEMPERATURE, 0, DATATYPE_NUMBER, 4, dht22_temperature);
			tcpip_write_sporadic(PARAM_HUMIDITY, 0, DATATYPE_NUMBER, 4, dht22_humidity);

			dht22_data_index = 0;
		}
	} else {
		PRINTF("Failed to read the DHT22 sensor\n");
	}
}

#endif

/*---------------------------------------------------------------------------*/
// CO2 UART1
/*---------------------------------------------------------------------------*/
#ifdef PLATFORM_PARABOX_DEV_HAS_CO2

#define CO2_SENSOR_AVR_LEN	4

uint32_t co2_concentration_series[CO2_SENSOR_AVR_LEN];
uint8_t co2_concentration_index = 0;

#define CO2_RESPONSE_LEN	9
static uint8_t co2_response[CO2_RESPONSE_LEN];
uint8_t co2_response_index = 0;

unsigned int uart1_send_bytes(const uint8_t *s, uint16_t len) {
	uint16_t i = 0;
	for(i = 0; i < len; i++) {
		uart_write_byte(1, s[i]);
	}
	return i;
}

/*---------------------------------------------------------------------------*/

static uint8_t calcCO2Checksum(uint8_t *packet) {
	uint8_t i;
	uint8_t checksum = 0;
	for(i = 1; i < 8; i++) {
		checksum += packet[i];
	}
	checksum = 0xFF - checksum;
	checksum += 1;
	return checksum;
}

/*---------------------------------------------------------------------------*/
static int uart_rx_callback(unsigned char c) {
	co2_response[co2_response_index] = c;
	++co2_response_index;
	if(co2_response_index == CO2_RESPONSE_LEN) {
		uint8_t checksum = calcCO2Checksum(co2_response);
		if(checksum == co2_response[8]) {
			uint32_t co2 = co2_response[2] * 256 + co2_response[3];
			PRINTF("CO2 series [%u] %lu PPM\n", co2_concentration_index, co2);

			co2_concentration_series[co2_concentration_index] = co2;
			co2_concentration_index++;

			if(co2_concentration_index == CO2_SENSOR_AVR_LEN) {
				uint32_t avr_co2 = 0;
				uint8_t i = 0;
				for(i = 0; i < CO2_SENSOR_AVR_LEN; i++) {
					avr_co2 += co2_concentration_series[i];
				}
				co2_concentration = avr_co2 / CO2_SENSOR_AVR_LEN;
				PRINTF("AVR CO2 GAS Concentration: %lu\n", co2_concentration);

				tcpip_write_sporadic(PARAM_CO2_CONCENTRATION, 0, DATATYPE_NUMBER, 4, co2_concentration);

				co2_concentration_index = 0;
			}
		} else {
			PRINTF("CO2 Bad checksum: %u, %u\n", checksum, co2_response[8]);
		}
		co2_response_index = 0;
	}
	return 0;
}

const uint8_t co2_request[] = { 0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79 };

static void processCO2Sensor() {
	co2_response_index = 0;
	uart1_send_bytes(co2_request, 9);
}

#endif

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_echo_server_process, ev, data) {
	PROCESS_BEGIN();

	PRINTF("Starting UDP Server\n");

	uiplib_ip6addrconv(server_ip_string, &server_ip6addr);
	PRINTF("Server address: ");PRINT6ADDR(&server_ip6addr);PRINTF("\n");

	print_local_addresses();

	server_conn = udp_new(NULL, UIP_HTONS(0), NULL);
	udp_bind(server_conn, UIP_HTONS(LOCAL_PORT));

	PRINTF("Listen port: %u, TTL=%u\n", LOCAL_PORT, server_conn->ttl);

	// Inputs configuration
	input_set_callback(input_fire_handler);
	etimer_set(&input_read_timer, DEFAULT_INPUT_READ_INTERVAL);

	// Ping configuration
	def_rt_rssi = 0x8000000;
	uip_icmp6_echo_reply_callback_add(&echo_reply_notification, echo_reply_handler);
	etimer_set(&echo_request_timer, DEFAULT_RSSI_MEAS_INTERVAL);

	// Telemetry configuration
	etimer_set(&telemetry_send_timer, DEFAULT_TELEMETRY_SEND_INTERVAL);

	// Sensor configuration
	etimer_set(&sensor_read_timer, DEFAULT_SENSOR_READ_INTERVAL);

	// DHT22
#ifdef PLATFORM_PARABOX_DEV_HAS_DHT22
	  SENSORS_ACTIVATE(dht22);
#endif

	// CO2 configuration
#ifdef PLATFORM_PARABOX_DEV_HAS_CO2
	uart_set_input(1, uart_rx_callback); //set the callback
#endif

#ifdef RECEIVE_BROADCAST
//	uip_ipaddr_t addr;
	simple_udp_register(&broadcast_connection, UDP_BROADCAST_PORT, NULL, UDP_BROADCAST_PORT, broadcast_receiver);
#endif

	leds_on(LEDS_YELLOW);

	while (1) {
		PROCESS_YIELD();
		//
		// Process TCP
		//
		if(ev == tcpip_event) {
			tcpip_handler();
		//
		// Process Inputs
		//
		} else if((ev == PROCESS_EVENT_TIMER) && (data == &input_read_timer)) {
			input_read();
			etimer_set(&input_read_timer, DEFAULT_INPUT_READ_INTERVAL);
		// Sensor event
		} else if((ev == PROCESS_EVENT_TIMER) && (data == &sensor_read_timer)) {

#ifdef PLATFORM_PARABOX_DEV_HAS_DHT22
			processDHT22Sensor();
#endif

#ifdef PLATFORM_PARABOX_DEV_HAS_CO2
			processCO2Sensor();
#endif

			etimer_set(&sensor_read_timer, DEFAULT_SENSOR_READ_INTERVAL);
		//
		// Process Ping
		//
		} else if((ev == PROCESS_EVENT_TIMER) && (data == &echo_request_timer)) {
			ping_parent();
			etimer_set(&echo_request_timer, DEFAULT_RSSI_MEAS_INTERVAL);
		//
		// Process telemetry
		//
		} else if((ev == PROCESS_EVENT_TIMER) && (data == &telemetry_send_timer)) {
			tcpip_write_sporadic(PARAM_RSSI, 0, DATATYPE_NUMBER, 4, def_rt_rssi);
			tcpip_write_sporadic(PARAM_UPTIME, 0, DATATYPE_NUMBER, 4, clock_seconds());
			etimer_set(&telemetry_send_timer, DEFAULT_TELEMETRY_SEND_INTERVAL);
		} else if((ev == sensors_event) && (data == INPUT_BOOT)) {
			PRINTF("Boot button pressed, enter bootloader...\n");
			start_bootloader();
		}
	}
	PROCESS_END();
}

