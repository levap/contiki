#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"

#include <string.h>

#define DEBUG DEBUG_PRINT

#include "net/ip/uip-debug.h"
#include "dev/leds.h"
#include "button-sensor.h"
#include "dev/cc2538-sensors.h"
#include "net/rpl/rpl.h"
#include "net/ipv6/uip-icmp6.h"
#include "net/ipv6/sicslowpan.h"
#include "dev/flash.h"
#include "dev/rom-util.h"
#include "dev/watchdog.h"
#include "cpu.h"
#include "lib/crc16.h"

#include "protocol/protocol.h"
/*---------------------------------------------------------------------------*/
#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
#define UIP_UDP_BUF  ((struct uip_udp_hdr *)&uip_buf[uip_l2_l3_hdr_len])

#define MAX_PAYLOAD_LEN 256
/*---------------------------------------------------------------------------*/
static struct uip_udp_conn *server_conn;
static char inputBuffer[MAX_PAYLOAD_LEN];
static char outputBuffer[MAX_PAYLOAD_LEN];
static uint16_t len;
/*---------------------------------------------------------------------------*/
PROCESS(udp_echo_server_process, "UDP Server Process");
AUTOSTART_PROCESSES(&udp_echo_server_process);

/*---------------------------------------------------------------------------*/
//static const char *server_ip_string = "fd00::1";
//static uip_ip6addr_t server_ip6addr;
//#define SERVER_PORT 	3001
#define LOCAL_PORT		3000

#define THING_VERSION	"01.01-02.00-BL"

#define INPUT_BOOT &button_boot_sensor

/*--------------------------------------------------------------------------*/
/* Firmware 																*/
/*--------------------------------------------------------------------------*/
static uint16_t crc16_calc = 0;
static uint16_t next_block_num = 0xFFFF;

struct __attribute__((__packed__)) firmware_hdr {
  uint32_t size;    		/* full size in bytes */
  uint32_t block_size;    	/* block size in bytes */
  uint32_t addr;    		/* physical load addr */
  uint32_t crc16;
} firmware_img_hdr;

/*---------------------------------------------------------------------------*/
extern const flash_cca_lock_page_t flash_cca_lock_page;

static void update_cca_startup_address(uint32_t addr) {
	flash_cca_lock_page_t flash_cca_saved = flash_cca_lock_page;
	uint32_t sector_start = ((uint32_t) &flash_cca_lock_page) & ~(FLASH_PAGE_SIZE - 1);
	flash_cca_saved.app_entry_point = (const void *) addr;
	INTERRUPTS_DISABLE();
	rom_util_page_erase(sector_start, FLASH_PAGE_SIZE);
	rom_util_program_flash((uint32_t *) &flash_cca_saved, (uint32_t) &flash_cca_lock_page, sizeof(flash_cca_lock_page_t));
	INTERRUPTS_ENABLE();
}

/*---------------------------------------------------------------------------*/
static struct ctimer reboot_timer;
static void do_the_reboot(void *ptr) {
	watchdog_reboot();
}

/*---------------------------------------------------------------------------*/
static void tcpip_write(const void *data, int len) {
	uip_ipaddr_copy(&server_conn->ripaddr, &UIP_IP_BUF->srcipaddr);
	server_conn->rport = UIP_UDP_BUF->srcport;

	uip_udp_packet_send(server_conn, data, len);

	/* Restore server connection to allow data from any node */
	uip_create_unspecified(&server_conn->ripaddr);
	server_conn->rport = 0;
}

/*---------------------------------------------------------------------------*/
static void tcpip_write_error(struct ParaboxHeader *messageHeader, uint8_t errorCode) {
	if(messageHeader->action == ACTION_WRITE) {
		messageHeader->action = ACTION_WRITE_ACK;
	} else if(messageHeader->action == ACTION_READ) {
		messageHeader->action = ACTION_READ_ACK;
	}
	messageHeader->dataType = DATATYPE_ERROR;
	messageHeader->dataLen = 1;
	memcpy(outputBuffer, messageHeader, sizeof(*messageHeader));
	outputBuffer[sizeof(*messageHeader)] = errorCode;

	tcpip_write(outputBuffer, sizeof(*messageHeader) + messageHeader->dataLen);
}

/*---------------------------------------------------------------------------*/
static void tcpip_handler(void) {
	struct ParaboxHeader messageHeader;
	memset(inputBuffer, 0, MAX_PAYLOAD_LEN);
	if (uip_newdata()) {
		len = uip_datalen();
		memcpy(inputBuffer, uip_appdata, len);
		PRINTF("%u bytes from [", len); PRINT6ADDR(&UIP_IP_BUF->srcipaddr); PRINTF("]:%u\n", UIP_HTONS(UIP_UDP_BUF->srcport));
		if (len >= sizeof(struct ParaboxHeader)) {
			memcpy(&messageHeader, inputBuffer, sizeof(struct ParaboxHeader));
			switch(messageHeader.parameter) {
			case PARAM_VERSION:
				if (messageHeader.action == ACTION_READ) {
					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_STRING;
					messageHeader.dataLen = sizeof(THING_VERSION);

					memcpy(outputBuffer, &messageHeader, sizeof(struct ParaboxHeader));
					int i = 0;
					for(i = 0; i < sizeof(THING_VERSION); ++i) {
						outputBuffer[sizeof(struct ParaboxHeader) + i] = THING_VERSION[i];
					}
					tcpip_write(outputBuffer, sizeof(struct ParaboxHeader) + messageHeader.dataLen);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_UPTIME:
				if (messageHeader.action == ACTION_READ) {
					messageHeader.action = ACTION_READ_ACK;
					messageHeader.dataType = DATATYPE_NUMBER;
					messageHeader.dataLen = 4;

					memcpy(outputBuffer, &messageHeader, sizeof(struct ParaboxHeader));
					unsigned long uptime = clock_seconds();
					outputBuffer[sizeof(struct ParaboxHeader)] = uptime & 0xFF;
					outputBuffer[sizeof(struct ParaboxHeader) + 1] = (uptime >> 8) & 0xFF;
					outputBuffer[sizeof(struct ParaboxHeader) + 2] = (uptime >> 16) & 0xFF;
					outputBuffer[sizeof(struct ParaboxHeader) + 3] = (uptime >> 24) & 0xFF;

					tcpip_write(outputBuffer, sizeof(struct ParaboxHeader) + messageHeader.dataLen);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_RESTART:
				if (messageHeader.action == ACTION_WRITE) {
					messageHeader.action = ACTION_WRITE_ACK;
					messageHeader.dataType = DATATYPE_VOID;
					messageHeader.dataLen = 0;
					tcpip_write(outputBuffer, sizeof(struct ParaboxHeader));

					// reboot after 2 sec
					ctimer_set(&reboot_timer, CLOCK_SECOND * 2, do_the_reboot, NULL);
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_FW_UPDATE_START:
				if (messageHeader.action == ACTION_WRITE) {
					crc16_calc = 0;
					next_block_num = 0;

					/* get the header */
					memcpy(&firmware_img_hdr, &inputBuffer[sizeof(struct ParaboxHeader)], sizeof(struct firmware_hdr));

					PRINTF("firmware address: 0x%lx\n", firmware_img_hdr.addr);
					PRINTF("firmware size: 0x%lx\n", firmware_img_hdr.size);
					PRINTF("firmware block size: 0x%lx\n", firmware_img_hdr.block_size);
					PRINTF("firmware crc16: 0x%lx\n", firmware_img_hdr.crc16);

					/* erase flash */
					INTERRUPTS_DISABLE();
					rom_util_page_erase((firmware_img_hdr.addr & ~(FLASH_PAGE_SIZE - 1)), firmware_img_hdr.size);
					INTERRUPTS_ENABLE();

					/* send ack */
					messageHeader.action = ACTION_WRITE_ACK;
					messageHeader.dataType = DATATYPE_VOID;
					messageHeader.dataLen = 0;
					tcpip_write(outputBuffer, sizeof(struct ParaboxHeader));
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_FW_UPDATE_DATA:
				if (messageHeader.action == ACTION_WRITE) {
					// reject wrong block
					if(messageHeader.index != next_block_num) {
						tcpip_write_error(&messageHeader, ERROR_WRONG_FW_BLOCKNUM);
						return;
					}
					uint8_t *flashed_data_addr = (uint8_t *) (firmware_img_hdr.addr + messageHeader.index * firmware_img_hdr.block_size);

					INTERRUPTS_DISABLE();
					rom_util_program_flash((uint32_t *) &inputBuffer[sizeof(struct ParaboxHeader)], (uint32_t) flashed_data_addr, messageHeader.dataLen);
					INTERRUPTS_ENABLE();

					int i;
					for(i = 0; i < messageHeader.dataLen; i++) {
					  crc16_calc = crc16_add(flashed_data_addr[i], crc16_calc);
					}
					next_block_num++;

					/* send ack */
					messageHeader.action = ACTION_WRITE_ACK;
					messageHeader.dataType = DATATYPE_VOID;
					messageHeader.dataLen = 0;
					tcpip_write(outputBuffer, sizeof(struct ParaboxHeader));
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			case PARAM_FW_UPDATE_END:
				if (messageHeader.action == ACTION_WRITE) {
					/* check checksum */
					if(crc16_calc == firmware_img_hdr.crc16) {
						update_cca_startup_address(firmware_img_hdr.addr);
						PRINTF("Firmware updated. Device will reboot!\n");

						/* send ack */
						messageHeader.action = ACTION_WRITE_ACK;
						messageHeader.dataType = DATATYPE_VOID;
						messageHeader.dataLen = 0;
						tcpip_write(outputBuffer, sizeof(struct ParaboxHeader));

						// reboot after 1 sec
						ctimer_set(&reboot_timer, CLOCK_SECOND, do_the_reboot, NULL);
					} else {
						tcpip_write_error(&messageHeader, ERROR_WRONG_FW_CRC);
					}
				} else {
					tcpip_write_error(&messageHeader, ERROR_WRONG_ACTION);
				}
				break;
			}
		}
	}
	return;
}

/*---------------------------------------------------------------------------*/
static void print_local_addresses(void) {
	int i;
	uint8_t state;
	PRINTF("Local IPv6 addresses:\n");
	for (i = 0; i < UIP_DS6_ADDR_NB; i++) {
		state = uip_ds6_if.addr_list[i].state;
		if (uip_ds6_if.addr_list[i].isused
				&& (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
			PRINTF("  ");PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);PRINTF("\n");
			if (state == ADDR_TENTATIVE) {
				uip_ds6_if.addr_list[i].state = ADDR_PREFERRED;
			}
		}
	}
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_echo_server_process, ev, data) {
	PROCESS_BEGIN();

	PRINTF("Starting UDP Server\n");
	print_local_addresses();

	server_conn = udp_new(NULL, UIP_HTONS(0), NULL);
	udp_bind(server_conn, UIP_HTONS(LOCAL_PORT));

	PRINTF("Listen port: %u, TTL=%u\n", LOCAL_PORT, server_conn->ttl);

	while (1) {
		PROCESS_YIELD();
		if(ev == tcpip_event) {
			tcpip_handler();
		} else if((ev == sensors_event) && (data == INPUT_BOOT)) {
			PRINTF("Boot button pressed\n");
		}
	}
	PROCESS_END();
}

