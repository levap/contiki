#ifndef PROJECT_CONF_H_
#define PROJECT_CONF_H_

/* Disabling TCP */
#undef UIP_CONF_TCP
#define UIP_CONF_TCP                   0

//-------------------------------------------------------------------------//
// MY CONFIG START
#undef NETSTACK_CONF_RDC
//#define NETSTACK_CONF_RDC     contikimac_driver
#define NETSTACK_CONF_RDC     nullrdc_driver

#undef NETSTACK_CONF_MAC
#define NETSTACK_CONF_MAC     csma_driver

//#undef LLSEC802154_CONF_ENABLED
//#define LLSEC802154_CONF_ENABLED          1
//
//#undef NETSTACK_CONF_FRAMER
//#define NETSTACK_CONF_FRAMER              noncoresec_framer
//
//#undef NETSTACK_CONF_LLSEC
//#define NETSTACK_CONF_LLSEC               noncoresec_driver
//
//#undef NONCORESEC_CONF_SEC_LVL
//#define NONCORESEC_CONF_SEC_LVL           0x02
//
//#define NONCORESEC_CONF_KEY { 0x67 , 0x53 , 0x11 , 0x9A , 0x6B , 0x05 , 0xEF , 0xC3 , 0x08 , 0x9A , 0x4C , 0xAA , 0x2C , 0x7D , 0x2E , 0x1F }

#define LPM_CONF_ENABLE       0
#define LPM_CONF_MAX_PM       1

#define IEEE802154_CONF_PANID           0x2222
#define CC2538_RF_CONF_CHANNEL              18
//-------------------------------------------------------------------------//
// MY CONFIG END

#define FLASH_CONF_FW_ADDR             (COFFEE_START + COFFEE_SIZE)

/* disable watchdog to alow flash erase/programing */
#define WATCHDOG_CONF_ENABLE           0

#endif /* PROJECT_CONF_H_ */
/*---------------------------------------------------------------------------*/
/**
 * @}
 * @}
 */
