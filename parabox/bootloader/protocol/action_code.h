/*
 * action_code.h
 *
 *  Created on: Oct 28, 2016
 *      Author: levap
 */

#ifndef PARABOX_DEVBOARD_PROTOCOL_ACTION_CODE_H_
#define PARABOX_DEVBOARD_PROTOCOL_ACTION_CODE_H_

#define ACTION_READ			1
#define ACTION_READ_ACK		2
#define ACTION_WRITE		3
#define ACTION_WRITE_ACK	4
#define ACTION_SPORADIC		5
#define ACTION_FIRMWARE_UPDATE			6
#define ACTION_FIRMWARE_UPDATE_ACK		7

#endif /* PARABOX_DEVBOARD_PROTOCOL_ACTION_CODE_H_ */
