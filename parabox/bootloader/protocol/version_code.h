/*
 * version_code.h
 *
 *  Created on: Oct 28, 2016
 *      Author: levap
 */

#ifndef PARABOX_DEVBOARD_PROTOCOL_VERSION_CODE_H_
#define PARABOX_DEVBOARD_PROTOCOL_VERSION_CODE_H_

#define PROTOCOL_VERSION		2

#endif /* PARABOX_DEVBOARD_PROTOCOL_VERSION_CODE_H_ */
